package siamux

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"path/filepath"
	"runtime"
	"strings"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"gitlab.com/NebulousLabs/Sia/build"
	"gitlab.com/NebulousLabs/Sia/persist"
	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/NebulousLabs/fastrand"
	"gitlab.com/NebulousLabs/siamux/helpers"
	"gitlab.com/NebulousLabs/siamux/mux"
)

type muxTester struct {
	*SiaMux
}

func testDir(name string) string {
	return filepath.Join(helpers.TestDir(name))
}

// publicKey returns the server's public key used to sign the initial handshake.
func (mt *muxTester) publicKey() mux.ED25519PublicKey {
	return mt.staticPubKey
}

// newMuxTester creates a new SiaMux which is ready to be used.
func newMuxTester(testDir string) (*muxTester, error) {
	path := filepath.Join(testDir, persist.RandomSuffix())
	sm, err := New("127.0.0.1:0", "127.0.0.1:0", persist.NewLogger(ioutil.Discard), path)
	if err != nil {
		return nil, err
	}
	return &muxTester{
		SiaMux: sm,
	}, nil
}

// newMuxTesterPair returns two new mux testers.
func newMuxTesterPair(name string) (client, server *muxTester) {
	// Create a client and a server.
	client, err := newMuxTester(testDir(name + "_client"))
	if err != nil {
		panic(err)
	}
	server, err = newMuxTester(testDir(name + "_server"))
	if err != nil {
		panic(err)
	}
	return client, server
}

// TestNewSiaMux confirms that creating and closing a SiaMux works as expected.
func TestNewSiaMux(t *testing.T) {
	// Create SiaMux.
	sm, err := newMuxTester(testDir(t.Name()))
	if err != nil {
		t.Fatal(err)
	}
	// Check if the appSeed was set.
	if sm.staticAppSeed == 0 {
		t.Error("appSeed is 0")
	}
	// Close it again.
	if err := sm.Close(); err != nil {
		t.Fatal(err)
	}
}

// TestNewStream tests if registering a listener and connecting to it works as
// expected.
func TestNewStream(t *testing.T) {
	// Run test with tcp conn.
	t.Run("TCP", func(t *testing.T) {
		client, server := newMuxTesterPair(t.Name())
		defer client.Close()
		defer server.Close()
		testNewStream(t, client, server, server.Address().String())
	})
	// Run test with websocket.
	t.Run("WS", func(t *testing.T) {
		client, server := newMuxTesterPair(t.Name())
		defer client.Close()
		defer server.Close()
		testNewStream(t, client, server, server.URL())
	})
}

// testNewStream test if registering a listener and connecting to it works as
// expected. The client will use srvAddr to connect to.
func testNewStream(t *testing.T, client, server *muxTester, srvAddr string) {
	// Prepare a handler to be registered by the server.
	var numHandlerCalls uint64
	handler := func(stream Stream) {
		atomic.AddUint64(&numHandlerCalls, 1)
		// Close the stream after handling it.
		if err := stream.Close(); err != nil {
			t.Fatal(err)
		}
	}
	// Try creating a new stream. This should fail with errUnknownSubscriber
	// since the server hasn't registered a handler yet.
	stream, err := client.NewStream("test", srvAddr, server.publicKey())
	if err != nil {
		t.Fatal(err)
	}
	_, err = stream.Read(make([]byte, 1))
	if err == nil || !strings.Contains(err.Error(), errUnknownSubscriber.Error()) {
		t.Fatal("error should be errUnknownSubscriber but was:", err)
	}
	if err := client.assertNumMuxs(1, 1, 1); err != nil {
		t.Fatal(err)
	}
	// Register a listener.
	if err := server.NewListener("test", handler); err != nil {
		t.Fatal(err)
	}
	if len(server.handlers) != 1 {
		t.Fatalf("expected %v handler but got %v", 1, len(server.handlers))
	}
	// Try creating a new stream again. This time it should work.
	stream, err = client.NewStream("test", srvAddr, server.publicKey())
	if err != nil {
		t.Fatal(err)
	}
	// Check the fields of client and server. The client still has 1 mux since
	// it's reusing the already open one.
	if err := client.assertNumMuxs(1, 1, 1); err != nil {
		t.Fatal(err)
	}
	if err := server.assertNumMuxs(1, 1, 0); err != nil {
		t.Fatal(err)
	}
	// Check if the handler has been called exactly once so far. Need to do this
	// in a retry to avoid NDFs.
	err = helpers.Retry(100, 100*time.Millisecond, func() error {
		if numCalls := atomic.LoadUint64(&numHandlerCalls); numCalls != 1 {
			return fmt.Errorf("handler should've been called once but was %v", numCalls)
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	// Delete the listener again. The stream should be closed.
	if err := server.CloseListener("test"); err != nil {
		t.Fatal(err)
	}
	if len(server.handlers) != 0 {
		t.Fatalf("expected %v handler but got %v", 0, len(server.handlers))
	}
	// Try creating a new stream one last time. This should fail with errUnknownSubscriber
	// since the server unregistered the handler.
	stream, err = client.NewStream("test", srvAddr, server.publicKey())
	if err != nil {
		t.Fatal(err)
	}
	_, err = stream.Read(make([]byte, 1))
	if err == nil || !strings.Contains(err.Error(), errUnknownSubscriber.Error()) {
		t.Fatal("error should be errUnknownSubscriber but was:", err)
	}
	// Close the stream.
	if err := stream.Close(); err != nil {
		t.Fatal(err)
	}
	// Check if the handler has been called exactly once again.
	if numCalls := atomic.LoadUint64(&numHandlerCalls); numCalls != 1 {
		t.Fatalf("handler should've been called once but was %v", numCalls)
	}
	// The server should still have 1 mux.
	if err := server.assertNumMuxs(1, 1, 0); err != nil {
		t.Fatal(err)
	}
	// Simulate a timeout by closing the server's mux.
	for _, mux := range server.muxs {
		if err := mux.Close(); err != nil {
			t.Fatal(err)
		}
	}
	// The server should be back to having 0 muxs since closing the mux caused
	// it to remove it from the SiaMux.
	if err := server.assertNumMuxs(0, 0, 0); err != nil {
		t.Fatal(err)
	}
	// Since the server terminated the connection, the client should also be
	// cleaned up.
	err = helpers.Retry(100, 100*time.Millisecond, func() error {
		return client.assertNumMuxs(0, 0, 0)
	})
	if err != nil {
		t.Error(err)
	}
}

// TestMultiStream tests that multiple streams talking over the same mux can all
// send frames at the same time, and all receive the expected data.
func TestMultiStream(t *testing.T) {
	// Run test with tcp conn.
	t.Run("TCP", func(t *testing.T) {
		client, server := newMuxTesterPair(t.Name())
		defer client.Close()
		defer server.Close()
		testMultiStream(t, client, server, server.Address().String())
	})
	// Run test with websocket.
	t.Run("WS", func(t *testing.T) {
		client, server := newMuxTesterPair(t.Name())
		defer client.Close()
		defer server.Close()
		testMultiStream(t, client, server, server.URL())
	})
}

// testMultiStream tests that multiple streams talking over the same mux can all
// send frames at the same time, and all receive the expected data.
func testMultiStream(t *testing.T, client, server *muxTester, srvAddr string) {
	// Create handlers for the streams on the server. Each handler reads an
	// expected set of bytes and then closes. Each handler is a dramatically
	// different size, meaning that they handle different sized frames and
	// different numbers of frames.
	handler1msg := fastrand.Bytes(fastrand.Intn(10) + 1)
	handler1resp := fastrand.Bytes(fastrand.Intn(10) + 1)
	handler2msg := fastrand.Bytes(fastrand.Intn(1e3) + 1)
	handler2resp := fastrand.Bytes(fastrand.Intn(1e3) + 1)
	handler3msg := fastrand.Bytes(fastrand.Intn(10e3) + 1)
	handler3resp := fastrand.Bytes(fastrand.Intn(10e3) + 1)
	handler4msg := fastrand.Bytes(fastrand.Intn(2e6) + 1)
	handler4resp := fastrand.Bytes(fastrand.Intn(2e6) + 1)
	handler1 := func(stream Stream) {
		defer func() {
			err := stream.Close()
			if err != nil {
				t.Error(err)
			}
		}()
		readBuf := make([]byte, len(handler1msg))
		_, err := io.ReadFull(stream, readBuf)
		if err != nil {
			t.Error(err)
			return
		}
		if !bytes.Equal(readBuf, handler1msg) {
			t.Error("unexpected mismatch")
			return
		}
		_, err = stream.Write(handler1resp)
		if err != nil {
			t.Error(err)
			return
		}
	}
	handler2 := func(stream Stream) {
		defer func() {
			err := stream.Close()
			if err != nil {
				t.Error(err)
			}
		}()
		readBuf := make([]byte, len(handler2msg))
		_, err := io.ReadFull(stream, readBuf)
		if err != nil {
			t.Error(err)
			return
		}
		if !bytes.Equal(readBuf, handler2msg) {
			t.Error("unexpected mismatch")
			return
		}
		_, err = stream.Write(handler2resp)
		if err != nil {
			t.Error(err)
			return
		}
	}
	handler3 := func(stream Stream) {
		defer func() {
			err := stream.Close()
			if err != nil {
				t.Error(err)
			}
		}()
		readBuf := make([]byte, len(handler3msg))
		_, err := io.ReadFull(stream, readBuf)
		if err != nil {
			t.Error(err)
			return
		}
		if !bytes.Equal(readBuf, handler3msg) {
			t.Error("unexpected mismatch")
			return
		}
		_, err = stream.Write(handler3resp)
		if err != nil {
			t.Error(err)
			return
		}
	}
	handler4 := func(stream Stream) {
		defer func() {
			err := stream.Close()
			if err != nil {
				t.Error(err)
			}
		}()
		readBuf := make([]byte, len(handler4msg))
		_, err := io.ReadFull(stream, readBuf)
		if err != nil {
			t.Error(err)
			return
		}
		if !bytes.Equal(readBuf, handler4msg) {
			t.Log()
			return
		}
		_, err = stream.Write(handler4resp)
		if err != nil {
			t.Error(err)
			return
		}
	}

	// Register all handlers to the server.
	err := server.NewListener("1", handler1)
	if err != nil {
		t.Fatal(err)
	}
	err = server.NewListener("2", handler2)
	if err != nil {
		t.Fatal(err)
	}
	err = server.NewListener("3", handler3)
	if err != nil {
		t.Fatal(err)
	}
	err = server.NewListener("4", handler4)
	if err != nil {
		t.Fatal(err)
	}

	// Create the sender methods.
	sender1 := func() {
		stream, err := client.NewStream("1", srvAddr, server.publicKey())
		if err != nil {
			t.Error(err)
			return
		}
		_, err = stream.Write(handler1msg)
		if err != nil {
			t.Error(err)
			return
		}
		readBuf := make([]byte, len(handler1resp))
		_, err = io.ReadFull(stream, readBuf)
		if err != nil {
			t.Error(err)
			return
		}
		if !bytes.Equal(readBuf, handler1resp) {
			t.Error("mismatch")
			return
		}
		err = stream.Close()
		if err != nil {
			t.Error(err)
		}
	}
	sender2 := func() {
		stream, err := client.NewStream("2", srvAddr, server.publicKey())
		if err != nil {
			t.Error(err)
			return
		}
		_, err = stream.Write(handler2msg)
		if err != nil {
			t.Error(err)
			return
		}
		readBuf := make([]byte, len(handler2resp))
		_, err = io.ReadFull(stream, readBuf)
		if err != nil {
			t.Error(err)
			return
		}
		if !bytes.Equal(readBuf, handler2resp) {
			t.Error("mismatch")
			return
		}
		err = stream.Close()
		if err != nil {
			t.Error(err)
		}
	}
	sender3 := func() {
		stream, err := client.NewStream("3", srvAddr, server.publicKey())
		if err != nil {
			t.Error(err)
			return
		}
		_, err = stream.Write(handler3msg)
		if err != nil {
			t.Error(err)
			return
		}
		readBuf := make([]byte, len(handler3resp))
		_, err = io.ReadFull(stream, readBuf)
		if err != nil {
			t.Error(err)
			return
		}
		if !bytes.Equal(readBuf, handler3resp) {
			t.Error("mismatch")
			return
		}
		err = stream.Close()
		if err != nil {
			t.Error(err)
		}
	}
	sender4 := func() {
		stream, err := client.NewStream("4", srvAddr, server.publicKey())
		if err != nil {
			t.Error(err)
			return
		}
		_, err = stream.Write(handler4msg)
		if err != nil {
			t.Error(err)
			return
		}
		readBuf := make([]byte, len(handler4resp))
		_, err = io.ReadFull(stream, readBuf)
		if err != nil {
			t.Error(err)
			return
		}
		if !bytes.Equal(readBuf, handler4resp) {
			t.Error("mismatch")
			return
		}
		err = stream.Close()
		if err != nil {
			t.Error(err)
		}
	}

	// Verify that without concurrency, all of the sender methods work.
	sender1()
	sender2()
	sender3()
	sender4()

	// Spin up multiple goroutines per handler to continuously send frames down
	// the mux.
	var wg sync.WaitGroup
	threadsPerSender := 3
	sendsPerSender := int(1e3)
	for i := 0; i < threadsPerSender; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for i := 0; i < sendsPerSender; i++ {
				sender1()
			}
		}()
	}
	for i := 0; i < threadsPerSender; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for i := 0; i < sendsPerSender/2; i++ {
				sender2()
			}
		}()
	}
	for i := 0; i < threadsPerSender; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for i := 0; i < sendsPerSender/5; i++ {
				sender3()
			}
		}()
	}
	for i := 0; i < threadsPerSender; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for i := 0; i < sendsPerSender/20; i++ {
				sender4()
			}
		}()
	}
	wg.Wait()

	// Both siamuxs should only contain a single mux each.
	if err := client.assertNumMuxs(1, 1, 1); err != nil {
		t.Fatal(err)
	}
	if err := server.assertNumMuxs(1, 1, 0); err != nil {
		t.Fatal(err)
	}
	var clientMux, serverMux muxInfo
	for _, mux := range client.muxSet {
		clientMux = mux
		break
	}
	for _, mux := range server.muxSet {
		serverMux = mux
		break
	}
	if len(clientMux.addresses) != 1 {
		t.Fatal("client should have 1 address: ", len(clientMux.addresses))
	}
	if len(serverMux.addresses) != 0 {
		t.Fatal("server should have 0 addresses: ", len(serverMux.addresses))
	}

	// Cleanly close muxs.
	err = client.Close()
	if err != nil {
		t.Error(err)
	}
	err = server.Close()
	if err != nil {
		t.Error(err)
	}
}

// TestTimeoutCallback makes sure the timeout callback methods are triggered
// correctly.
func TestTimeoutCallback(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	mux.DefaultMaxTimeout = 5
	mux.LowerMaxTimeout = 0
	mux.DefaultMaxStreamTimeout = 3
	mux.TimeoutNotificationBuffer = time.Duration(4 * time.Second)

	// Create 2 SiaMuxs and have one of them connect to the other one.
	client, err := newMuxTester(testDir(t.Name()))
	if err != nil {
		t.Fatal(err)
	}
	server, err := newMuxTester(testDir(t.Name()))
	if err != nil {
		t.Fatal(err)
	}

	// Prepare a handler to be registered by the server.
	data := fastrand.Bytes(10)
	handler := func(stream Stream) {
		// Read some data from the stream.
		d := make([]byte, len(data))
		_, err := stream.Read(d)
		if err != nil {
			t.Error(err)
		}
		if !bytes.Equal(d, data) {
			t.Errorf("data doesn't match")
		}
		// Close the stream after handling it.
		if err := stream.Close(); err != nil {
			t.Error(err)
		}
	}
	if err := server.NewListener("test", handler); err != nil {
		t.Fatal(err)
	}
	// Try creating a new stream. This should fail with errUnknownSubscriber
	// since the server hasn't registered a handler yet.
	stream, err := client.NewStream("test", server.Address().String(), server.publicKey())
	if err != nil {
		t.Fatal(err)
	}
	// Write some data to the stream to make sure it's established.
	_, err = stream.Write(data)
	if err != nil {
		t.Fatal(err)
	}
	// Wait for 5 times the max timeout without writing or reading to make sure
	// the connection is kept alive.
	time.Sleep(5 * time.Duration(mux.DefaultMaxTimeout) * time.Second)
	// The mux should still be in outgoingMuxs
	client.staticMu.Lock()
	m, exists := client.outgoingMuxs[server.Address().String()]
	client.staticMu.Unlock()
	if !exists {
		t.Fatal("outgoing mux doesn't exist anymore")
	}
	// Directly use the mux to check if it's still working.
	stream, err = m.NewStream()
	if err != nil {
		t.Fatal(err)
	}
	// Write to the stream. This should still work.
	_, err = stream.Write(data)
	if err != nil {
		t.Fatal(err)
	}
}

// BenchmarkStreamThroughput tests the throughput of multiple threads sending
// data to a mux.
//
// Results
//
// 16 threads - i9 - Commit f69100d0d09b561e3819bfcd5b7802b562b4d5ca
//   TCP: 47.12 MB/s
//   WS : 27.98 MB/s
//
func BenchmarkStreamThroughput(b *testing.B) {
	b.Run("TCP", func(b *testing.B) {
		client, server := newMuxTesterPair(b.Name())
		defer client.Close()
		defer server.Close()
		benchmarkStreamThroughput(b, client, server, server.Address().String())
	})
	b.Run("WS", func(b *testing.B) {
		client, server := newMuxTesterPair(b.Name())
		defer client.Close()
		defer server.Close()
		benchmarkStreamThroughput(b, client, server, server.URL())
	})
}

// benchmarkStreamThroughput is the subtest run by BenchmarkStreamThroughput.
func benchmarkStreamThroughput(b *testing.B, client, server *muxTester, srvAddr string) {
	// Prepare a handler to be registered by the server.
	handler := func(stream Stream) {
		// Read from the stream.
		_, err := io.Copy(ioutil.Discard, stream)
		if errors.Contains(err, io.ErrClosedPipe) {
			return // shutdown
		}
		if err != nil {
			b.Error(err)
		}
	}
	if err := server.NewListener("test", handler); err != nil {
		b.Fatal(err)
	}

	// Declare some vars.
	numThreads := runtime.NumCPU()
	bytesPerOperation := numThreads * 10 * 1 << 20 // 10 MiB * numThreads
	b.SetBytes(int64(bytesPerOperation))
	data := fastrand.Bytes(bytesPerOperation)

	// Declare the sending thread.
	start := make(chan struct{})
	sender := func(stream Stream) {
		<-start
		// Write data
		for i := 0; i < b.N; i++ {
			_, err := stream.Write(data)
			if err != nil {
				b.Error(err)
				return
			}
		}
		// Close the stream.
		err := stream.Close()
		if err != nil {
			b.Error(err)
			return
		}
	}

	// start pool
	var wg sync.WaitGroup
	for i := 0; i < numThreads; i++ {
		// Connect to the server.
		stream, err := client.NewStream("test", srvAddr, server.publicKey())
		if err != nil {
			b.Fatal(err)
			return
		}
		wg.Add(1)
		go func() {
			sender(stream)
			wg.Done()
		}()
	}

	// reset timer
	b.ResetTimer()

	// start the threads
	close(start)

	// wait for them to finish
	wg.Wait()

	// log some info
	b.Logf("Ran test with %v threads", numThreads)
}

// TestAppSeedDerivation tests that both the client and server store the mux
// using the correct appSeed.
func TestAppSeedDerivation(t *testing.T) {
	// Create 2 SiaMuxs and have one of them connect to the other one.
	client, err := newMuxTester(testDir(t.Name()))
	if err != nil {
		t.Fatal(err)
	}
	server, err := newMuxTester(testDir(t.Name()))
	if err != nil {
		t.Fatal(err)
	}

	// Prepare a handler to be registered by the server.
	var numHandlerCalls uint64
	handler := func(stream Stream) {
		atomic.AddUint64(&numHandlerCalls, 1)
		// Close the stream after handling it.
		if err := stream.Close(); err != nil {
			t.Fatal(err)
		}
	}
	// Register a listener.
	if err := server.NewListener("test", handler); err != nil {
		t.Fatal(err)
	}
	if len(server.handlers) != 1 {
		t.Fatalf("expected %v handler but got %v", 1, len(server.handlers))
	}
	// Create a stream.
	stream, err := client.NewStream("test", server.Address().String(), server.publicKey())
	if err != nil {
		t.Fatal(err)
	}

	// Write some data to establish the stream on the server.
	_, err = stream.Write([]byte{1})
	if err != nil {
		t.Fatal(err)
	}

	// Both SiaMuxs should store a single mux with the same appSeed.
	clientSeed, err := deriveEphemeralAppSeed(client.staticAppSeed, server.Address())
	if err != nil {
		t.Fatal(err)
	}
	serverSeed, err := deriveEphemeralAppSeed(server.staticAppSeed, client.Address())
	if err != nil {
		t.Fatal(err)
	}
	expectedSeed := clientSeed + serverSeed

	_, exists := client.muxs[expectedSeed]
	if !exists {
		t.Fatal("client doesn't contain expected seed", expectedSeed)
	}
	_, exists = server.muxs[expectedSeed]
	if !exists {
		t.Fatal("server doesn't contain expected seed", expectedSeed)
	}
}

// testMuxCleanupOnDisconnect makes sure that a mux disconnecting from another
// mux will correctly clean up the other mux.
func testMuxCleanupOnDisconnect(t *testing.T, closeServer bool) {
	// Create 2 SiaMuxs and have one of them connect to the other one.
	client, err := newMuxTester(testDir(t.Name()))
	if err != nil {
		t.Fatal(err)
	}
	server, err := newMuxTester(testDir(t.Name()))
	if err != nil {
		t.Fatal(err)
	}

	// Prepare a handler to be registered by the server.
	var numHandlerCalls uint64
	handler := func(stream Stream) {
		atomic.AddUint64(&numHandlerCalls, 1)
		// Close the stream after handling it.
		if err := stream.Close(); err != nil {
			t.Fatal(err)
		}
	}
	// Register a listener.
	if err := server.NewListener("test", handler); err != nil {
		t.Fatal(err)
	}
	if len(server.handlers) != 1 {
		t.Fatalf("expected %v handler but got %v", 1, len(server.handlers))
	}
	// Create a new stream.
	stream, err := client.NewStream("test", server.Address().String(), server.publicKey())
	if err != nil {
		t.Fatal(err)
	}

	// Write some data to establish the stream on the server.
	_, err = stream.Write([]byte{1})
	if err != nil {
		t.Fatal(err)
	}

	// Check client and server muxs.
	err = client.assertNumMuxs(1, 1, 1)
	if err != nil {
		t.Fatal(err)
	}
	err = server.assertNumMuxs(1, 1, 0)
	if err != nil {
		t.Fatal(err)
	}

	// Close either the server or client mux.
	if closeServer {
		err = server.Close()
	} else {
		err = client.Close()
	}
	if err != nil {
		t.Fatal(err)
	}

	// Check client and server muxs again.
	err = build.Retry(100, time.Millisecond*100, func() error {
		if err := client.assertNumMuxs(0, 0, 0); err != nil {
			return err
		}
		if err := server.assertNumMuxs(0, 0, 0); err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
}

// TestMuxCleanupOnDisconnect makes sure that a mux disconnecting from another
// mux will correctly clean up the other mux.
func TestMuxCleanupOnDisconnect(t *testing.T) {
	t.Run("Server", func(t *testing.T) {
		testMuxCleanupOnDisconnect(t, true)
	})
	t.Run("Client", func(t *testing.T) {
		testMuxCleanupOnDisconnect(t, false)
	})
}

// assertNumMuxs is a helper to assert the number of muxs stored in the free
// containers of the siamux.
func (sm *SiaMux) assertNumMuxs(muxs, muxSet, outgoingMuxs int) error {
	sm.staticMu.Lock()
	defer sm.staticMu.Unlock()
	if len(sm.muxs) != muxs {
		return fmt.Errorf("sm.muxs: expected %v but got %v", muxs, len(sm.muxs))
	}
	if len(sm.muxSet) != muxSet {
		return fmt.Errorf("sm.muxSet: expected %v but got %v", muxSet, len(sm.muxSet))
	}
	if len(sm.outgoingMuxs) != outgoingMuxs {
		return fmt.Errorf("sm.outgoingMuxs: expected %v but got %v", outgoingMuxs, len(sm.outgoingMuxs))
	}
	return nil
}
