package helpers

import (
	"os"
	"path/filepath"
	"time"
)

var (
	// SiaMuxTestingDir is the directory that contains all of the files and
	// folders created during testing.
	SiaMuxTestingDir = filepath.Join(os.TempDir(), "SiaMuxTesting")
)

// Retry will call 'fn' 'tries' times, waiting 'durationBetweenAttempts'
// between each attempt, returning 'nil' the first time that 'fn' returns nil.
// If 'nil' is never returned, then the final error returned by 'fn' is
// returned.
func Retry(tries int, durationBetweenAttempts time.Duration, fn func() error) (err error) {
	for i := 1; i < tries; i++ {
		err = fn()
		if err == nil {
			return nil
		}
		time.Sleep(durationBetweenAttempts)
	}
	return fn()
}

// TestDir joins the provided directories and prefixes them with the Sia testing
// directory.
func TestDir(dirs ...string) string {
	path := filepath.Join(SiaMuxTestingDir, filepath.Join(dirs...))
	err := os.RemoveAll(path) // remove old test data
	if err != nil {
		panic(err)
	}
	return path
}
