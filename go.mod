module gitlab.com/NebulousLabs/siamux

go 1.13

require (
	github.com/gorilla/websocket v1.4.2
	gitlab.com/NebulousLabs/Sia v1.4.1
	gitlab.com/NebulousLabs/errors v0.0.0-20171229012116-7ead97ef90b8
	gitlab.com/NebulousLabs/fastrand v0.0.0-20181126182046-603482d69e40
	go.etcd.io/bbolt v1.3.4 // indirect
	golang.org/x/crypto v0.0.0-20200117160349-530e935923ad
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859
)
