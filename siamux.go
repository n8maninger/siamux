package siamux

import (
	"context"
	"encoding/binary"
	"fmt"
	"math"
	"net"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/NebulousLabs/Sia/crypto"
	"gitlab.com/NebulousLabs/Sia/persist"
	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/NebulousLabs/fastrand"
	"gitlab.com/NebulousLabs/siamux/mux"
)

const (
	// DefaultNewStreamTimeout defines the amount of time that a stream is given
	// as a default timeout.
	DefaultNewStreamTimeout = time.Minute * 5
)

// TODO: Some features/tests that came to mind which we need
// - auto connect on siamux creation
type (
	// SiaMux needs a docstring
	SiaMux struct {
		handlers      map[string]Handler   // registered handlers for incoming connections
		outgoingMuxs  map[string]*mux.Mux  // maps addresses of outgoing connection to the mux that is wrapping them
		muxs          map[appSeed]*mux.Mux // all incoming and outgoing muxs
		muxSet        map[*mux.Mux]muxInfo // control set to confirm that every mux only has a single seed associated to it
		staticAppSeed appSeed              // random appSeed to uniquely identify application
		staticPubKey  mux.ED25519PublicKey
		staticPrivKey mux.ED25519SecretKey

		// fields related to TCP
		staticListener net.Listener // listener used to listen for incoming SiaMux requests

		// fields related to Websocket
		staticHTTPServer http.Server
		staticURL        string

		// utilities
		staticLog        *persist.Logger
		staticMu         sync.Mutex
		staticPersistDir string
		staticWG         sync.WaitGroup

		staticCtx       context.Context
		staticCtxCancel context.CancelFunc
	}
	// Stream is the interface for a Stream returned by the SiaMux. It's a
	// net.Conn with a few exceptions.
	Stream interface {
		// A stream implements net.Conn.
		net.Conn

		// Limit gets the limit on the stream.
		Limit() mux.BandwidthLimit

		// SetLimit sets a custom limit on the stream and initiates it to continue from
		// where the last limit left off.
		SetLimit(limit mux.BandwidthLimit) error

		// SetPriority allows for prioritizing individual streams over others.
		SetPriority(int) error
	}
	// Handler is a method that gets called whenever a new stream is accepted.
	Handler func(stream Stream)

	muxInfo struct {
		addresses []string
		appSeed   appSeed
	}

	// blockedStream is a wrapper around the mux.Stream which blocks Read
	// operations until the underlying channel is closed. It is used in
	// NewStream to block the user from reading from the stream before the
	// subscription response is received.
	blockedStream struct {
		// The underlying stream.
		*mux.Stream

		// errAvail is a channel which blocks until the error has been written
		// by the background process. For the blockedStream, all read calls are
		// blocked until errAvail has been closed.
		//
		// externErr is an error which can only be set by the background thread
		// waiting for the response from the peer on whether this stream has a
		// valid subscriber. If the err is 'nil' after errAvail is closed, this
		// means the stream is good.
		errAvail  chan struct{}
		externErr error
	}
)

// Read will block until `errAvail` is closed. The first time Read is called
// after creating the stream it will return the result of the subscription
// response or an error which occurred during the opening of the stream. If that
// err is != nil the underlying stream is already closed.
func (bs *blockedStream) Read(b []byte) (int, error) {
	// Block all reads until the errAvail chan has been closed.
	<-bs.errAvail
	if bs.externErr != nil {
		return 0, bs.externErr
	}

	// Pass the read through to the stream.
	return bs.Stream.Read(b)
}

// New creates a new SiaMux which listens on 'address' for incoming connections.
// pubKey and privKey will be used to sign the message during the initial
// handshake for incoming connections.
func New(tcpAddr, wsAddr string, log *persist.Logger, persistDir string) (*SiaMux, error) {
	// Create a client since it's the same as creating a server.
	ctx, cancel := context.WithCancel(context.Background())
	sm := &SiaMux{
		staticAppSeed:    appSeed(fastrand.Uint64n(math.MaxUint64)),
		staticLog:        log,
		handlers:         make(map[string]Handler),
		muxs:             make(map[appSeed]*mux.Mux),
		outgoingMuxs:     make(map[string]*mux.Mux),
		muxSet:           make(map[*mux.Mux]muxInfo),
		staticPersistDir: persistDir,
		staticCtx:        ctx,
		staticCtxCancel:  cancel,
	}
	// Init the persistence.
	if err := sm.initPersist(); err != nil {
		return nil, errors.AddContext(err, "failed to initialize SiaMux persistence")
	}
	// Spawn the listening thread.
	err := sm.spawnListeners(tcpAddr, wsAddr)
	if err != nil {
		return nil, errors.AddContext(err, "unable to spawn listener for siamux")
	}
	return sm, nil
}

// Address returns the underlying TCP listener's address.
func (sm *SiaMux) Address() net.Addr {
	return sm.staticListener.Addr()
}

// URL returns the underlying webserver's URL.
func (sm *SiaMux) URL() string {
	return sm.staticURL
}

// PublicKey returns the siamux's public key.
func (sm *SiaMux) PublicKey() mux.ED25519PublicKey {
	return sm.staticPubKey
}

// PrivateKey returns the siamux's private key.
func (sm *SiaMux) PrivateKey() mux.ED25519SecretKey {
	return sm.staticPrivKey
}

// Close closes the mux and waits for background threads to finish.
func (sm *SiaMux) Close() error {
	sm.staticCtxCancel()
	sm.staticWG.Wait()
	return nil
}

// NewStream connects to an address if not yet connected and opens a new
// stream to the given subscriber.
func (sm *SiaMux) NewStream(subscriber, address string, expectedPubKey mux.ED25519PublicKey) (Stream, error) {
	return sm.NewStreamTimeout(subscriber, address, DefaultNewStreamTimeout, expectedPubKey)
}

// NewStreamTimeout connects to an address if not yet connected and opens a new
// stream to the given subscriber. In case a new TCP connection needs to be
// established, the dialing process will time out after the specified timeout.
func (sm *SiaMux) NewStreamTimeout(subscriber, address string, timeout time.Duration, expectedPubKey mux.ED25519PublicKey) (Stream, error) {
	// Check if the subscriber is an empty string. It should be valid to
	// subscribe to the empty string but to avoid developer errors we check for
	// it here on the "client" side.
	if subscriber == "" {
		return nil, errors.New("subscriber can't be empty string")
	}

	// Check if an outgoing mux exists already.
	sm.staticMu.Lock()
	m, exists := sm.outgoingMuxs[address]
	sm.staticMu.Unlock()
	var err error
	if !exists {
		m, err = sm.managedNewOutgoingMux(address, timeout, expectedPubKey)
	}
	if err != nil {
		return nil, errors.AddContext(err, "unable to make a new outgoing mux")
	}

	// Create a new stream to subscribe.
	stream, err := m.NewStream()
	if err != nil {
		return nil, errors.AddContext(err, "unable to make a new outgoing stream")
	}
	// Send the subscriber request to tell the other SiaMux what name we want to
	// subscribe to.
	err = writeSubscriberRequest(stream, subscriber)
	if err != nil {
		err = errors.AddContext(err, "unable to write subscriber request")
		return nil, errors.Compose(err, stream.Close())
	}
	// Read the response from the other SiaMux.
	bs := &blockedStream{
		Stream: stream,

		errAvail: make(chan struct{}),
	}
	go func() {
		defer close(bs.errAvail)
		subscribeErr, err := readSubscriberResponse(stream)
		if err != nil {
			err = errors.AddContext(err, "unable to read subscriber response")
			bs.externErr = errors.Compose(err, stream.Close())
			return
		}
		if subscribeErr != nil {
			subscribeErr = errors.AddContext(subscribeErr, "subscriber error reported by peer")
			bs.externErr = errors.Compose(subscribeErr, stream.Close())
			return
		}
	}()
	return bs, nil
}

// addMux adds a mux to all of the SiaMuxs data structures.
func (sm *SiaMux) addMux(mux *mux.Mux, appSeed appSeed, addresses []string) {
	sm.muxSet[mux] = muxInfo{
		addresses: addresses,
		appSeed:   appSeed,
	}
	sm.muxs[appSeed] = mux
	for _, address := range addresses {
		sm.outgoingMuxs[address] = mux
	}
}

// managedAddMux adds a mux to all of the SiaMuxs data structures.
func (sm *SiaMux) managedAddMux(mux *mux.Mux, appSeed appSeed, addresses []string) {
	sm.staticMu.Lock()
	defer sm.staticMu.Unlock()
	sm.addMux(mux, appSeed, addresses)
}

// managedMuxCallback is called whenever one of the SiaMux's muxs is closed for
// any reason. It will handle necessary cleanup for the SiaMux.
func (sm *SiaMux) managedMuxCallback(mux *mux.Mux) {
	sm.managedRemoveMux(mux)
}

// managedTimeoutCallback is called whenever one of the SiaMux's muxs is close
// to timing out. This should check if a mux is worth keeping alive and then
// send a manual keepalive signal.
func (sm *SiaMux) managedTimeoutCallback(mux *mux.Mux) {
	sm.staticMu.Lock()
	mi, exists := sm.muxSet[mux]
	sm.staticMu.Unlock()
	if !exists {
		return // mux was removed in meantime
	}
	if len(mi.addresses) == 0 {
		return // not outgoing mux
	}
	if err := mux.Keepalive(); err != nil {
		err = errors.Compose(err, mux.Close())
		sm.staticLog.Print("managedTimeoutCallback: failed to send keepalive", err)
		return
	}
}

// managedNewOutgoingMux creates a new mux which is connected to address.
func (sm *SiaMux) managedNewOutgoingMux(address string, timeout time.Duration, expectedPubKey mux.ED25519PublicKey) (_ *mux.Mux, err error) {
	// If not, establish a new connection and wrap it in a mux.
	var conn net.Conn
	conn, err = dial(address, timeout)
	if err != nil {
		return nil, errors.AddContext(err, "unable to dial peer")
	}
	defer func() {
		if err != nil {
			err = errors.Compose(err, conn.Close())
		}
	}()
	// Wrap the connection in a mux.
	var m *mux.Mux
	m, err = mux.NewClientMux(sm.staticCtx, conn, expectedPubKey, sm.staticLog, sm.managedMuxCallback, sm.managedTimeoutCallback)
	if err != nil {
		return nil, errors.AddContext(err, "unable to create new client mux")
	}
	defer func() {
		if err != nil {
			err = errors.Compose(err, m.Close())
		}
	}()
	// Create a new stream to send the seed.
	stream, err := m.NewStream()
	if err != nil {
		return nil, errors.AddContext(err, "unable to create new stream")
	}
	defer func() {
		err = errors.Compose(err, stream.Close())
	}()
	// Derive an ephemeral seed for the peer.
	ephemeralSeed, err := deriveEphemeralAppSeed(sm.staticAppSeed, conn.RemoteAddr())
	if err != nil {
		return nil, errors.AddContext(err, "unable to derive ephemeral app seed")
	}
	// Send the seed request.
	err = writeSeedRequest(stream, ephemeralSeed)
	if err != nil {
		return nil, errors.AddContext(err, "unable to write seed request")
	}
	// Receive the seed response.
	peerSeed, err := readSeedResponse(stream)
	if err != nil {
		return nil, errors.AddContext(err, "unable to read seed response")
	}
	appSeed := ephemeralSeed + peerSeed

	// Add the mux.
	sm.managedAddMux(m, appSeed, []string{address})
	// Start accepting streams on that mux.
	sm.staticWG.Add(1)
	go func() {
		sm.threadedAccept(m)
		sm.staticWG.Done()
	}()
	return m, nil
}

// managedRemoveMux removes a mux from all of the SiaMuxs data structures.
func (sm *SiaMux) managedRemoveMux(mux *mux.Mux) {
	sm.staticMu.Lock()
	defer sm.staticMu.Unlock()
	sm.removeMux(mux)
}

// removeMux removes a mux from all of the SiaMuxs data structures.
func (sm *SiaMux) removeMux(mux *mux.Mux) {
	muxInfo, exists := sm.muxSet[mux]
	if !exists {
		sm.staticLog.Debug("WARN: tried to remove non-existent mux")
		return // mux wasn't added to the map yet.
	}
	delete(sm.muxSet, mux)
	delete(sm.muxs, muxInfo.appSeed)
	for _, address := range muxInfo.addresses {
		delete(sm.outgoingMuxs, address)
	}
}

// managedUpgradeConn upgrades an incoming net.Conn to a mux and adds it to the
// SiaMux.
func (sm *SiaMux) managedUpgradeConn(conn net.Conn) (err error) {
	// Upgrade the connection using a mux.
	mux, err := mux.NewServerMux(sm.staticCtx, conn, sm.staticPubKey, sm.staticPrivKey, sm.staticLog, sm.managedMuxCallback, sm.managedTimeoutCallback)
	if err != nil {
		sm.staticLog.Print("threadedListen: failed to create server mux", err)
		return
	}
	defer func() {
		if err != nil {
			err = errors.Compose(err, mux.Close())
		}
	}()
	// Get the first stream which contains the seed.
	seedStream, err := mux.AcceptStream()
	if err != nil {
		sm.staticLog.Print("threadedListen: failed to accept seed stream", err)
		return
	}
	defer func() {
		err = errors.Compose(err, seedStream.Close())
	}()
	// Read the seed request and derive an ephemeral seed.
	peerSeed, err := readSeedRequest(seedStream)
	if err != nil {
		sm.staticLog.Print("threadedListen: failed to read seed request", err)
		return
	}
	ephemeralSeed, err := deriveEphemeralAppSeed(sm.staticAppSeed, conn.RemoteAddr())
	if err != nil {
		sm.staticLog.Print("threadedListen: failed to derive shared seed", err)
		return
	}
	appSeed := ephemeralSeed + peerSeed
	// Check if the seed exists already. If it does, we close the existing
	// mux and use the new one without error.
	sm.managedReplaceMux(mux, appSeed)
	// Write response.
	err = writeSeedResponse(seedStream, ephemeralSeed)
	if err != nil {
		sm.staticLog.Print("threadedListen: failed to write seed response", err)
		return
	}

	// Spawn a thread to start listening on the mux.
	sm.staticWG.Add(1)
	go func() {
		sm.threadedAccept(mux)
		sm.managedRemoveMux(mux)
		sm.staticWG.Done()
	}()
	return
}

// threadedHandleTCP starts listening for incoming TCP connections which are
// then upgraded using the mux.Mux.
func (sm *SiaMux) threadedHandleTCP(listener net.Listener) {
	for {
		// Accept new connection.
		conn, err := listener.Accept()
		if err != nil {
			// shutdown
			break
		}
		// Upgrade the connection using a mux.
		err = sm.managedUpgradeConn(conn)
		if err != nil {
			err = errors.Compose(err, conn.Close())
			sm.staticLog.Printf("failed to upgrade conn: %v", err)
			continue
		}
	}
}

// handleWS handles incoming websocket connections by upgrading them to a
// mux.Mux.
func (sm *SiaMux) handleWS(w http.ResponseWriter, req *http.Request) {
	upgrader := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}
	conn, err := upgrader.Upgrade(w, req, nil)
	if err != nil {
		fmt.Println("Failed to upgrade connection", err)
		return
	}

	// Upgrade the connection using a mux.
	err = sm.managedUpgradeConn(mux.NewWSConn(conn))
	if err != nil {
		err = errors.Compose(err, conn.Close())
		sm.staticLog.Printf("failed to upgrade wsconn: %v", err)
		return
	}
}

// managedReplaceMux adds a mux to the Siamux, replacing any mux with the same
// appSeed.
func (sm *SiaMux) managedReplaceMux(mux *mux.Mux, appSeed appSeed) {
	sm.staticMu.Lock()
	m, exists := sm.muxs[appSeed]
	if exists {
		sm.removeMux(m)
		defer func() {
			err := m.Close()
			sm.staticLog.Printf("threadedListen: WARN: overwriting mux for seed %v, %v", appSeed, err)
		}()
	}
	sm.addMux(mux, appSeed, nil)
	// Note we do not defer a call to Unlock in order to ensure `m.Close` is
	// called outside of the lock in case a mux with the same seed already
	// existed.
	sm.staticMu.Unlock()
	return
}

// deriveEphemeralAppSeed derives an ephemeral appSeed for a peer.
func deriveEphemeralAppSeed(seed appSeed, address net.Addr) (appSeed, error) {
	host, _, err := net.SplitHostPort(address.String())
	if err != nil {
		return 0, err
	}
	rawSeed := crypto.HashAll(seed, host)
	return appSeed(binary.LittleEndian.Uint64(rawSeed[:])), nil
}

// dial is a helper method to dial a peer. It supports dialing websockets or raw
// tcp ports.
func dial(address string, timeout time.Duration) (net.Conn, error) {
	// Check for Websocket url.
	if strings.HasPrefix(address, "ws://") || strings.HasPrefix(address, "wss://") {
		ctx := context.Background()
		if timeout > 0 {
			cwt, cancel := context.WithTimeout(context.Background(), timeout)
			defer cancel()
			ctx = cwt
		}
		c, _, err := websocket.DefaultDialer.DialContext(ctx, address, nil)
		if err != nil {
			return nil, errors.AddContext(err, "failed to dial websocket")
		}
		return mux.NewWSConn(c), nil
	}
	// Default to TCP.
	conn, err := net.DialTimeout("tcp", address, timeout)
	if err != nil {
		return nil, errors.AddContext(err, "failed to dial tcp port")
	}
	return conn, nil
}
